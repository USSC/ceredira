package com.unixshaman.ussc.environments;

import com.unixshaman.ussc.ui.dialog.ShowErrorDialog;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import javax.xml.bind.annotation.*;
import java.io.File;
import java.util.ArrayList;
import java.util.List;

@XmlAccessorType()
@XmlRootElement(name = "parametersfile")
@XmlType(name = "parametersfile", propOrder = {"pathToFile", "environmentParameters"})
public class ParametersFile {
    private String pathToFile;
    private List<EnvironmentParameter> environmentParameters = new ArrayList<>();

    public String getPathToFile() {
        return pathToFile;
    }

    @XmlAttribute
    public void setPathToFile(String pathToFile) {
        this.pathToFile = pathToFile;
    }

    public List<EnvironmentParameter> getEnvironmentParameters() {
        return environmentParameters;
    }

    @XmlElement(name = "environmentparameter")
    public void setEnvironmentParameters(List<EnvironmentParameter> environmentParameters) {
        this.environmentParameters = environmentParameters;
    }

    public void addEnvironmentParameters(List<EnvironmentParameter> environmentParameters) {
        this.environmentParameters.addAll(environmentParameters);
    }

    public ParametersFile() {

    }

    public ParametersFile(String fileName) {
        this.pathToFile = fileName;

        try {
            File file = new File(fileName);
            JAXBContext jaxbContext = JAXBContext.newInstance(Environment.class);

            Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();
            ParametersFile parametersFile = (ParametersFile) jaxbUnmarshaller.unmarshal(file);

            this.pathToFile = parametersFile.pathToFile;
            this.environmentParameters = parametersFile.environmentParameters;
        } catch (JAXBException e) {
            ShowErrorDialog.showErrorDialog(e);
        }
    }

    public void saveParametersFile() {
        saveParametersFile(this.pathToFile);
    }

    public void saveParametersFile(String fileName) {
        try {
            File file = new File(fileName);
            file.getParentFile().mkdirs();
            JAXBContext jaxbContext = JAXBContext.newInstance(ParametersFile.class);
            Marshaller jaxbMarshaller = jaxbContext.createMarshaller();

            jaxbMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);

            jaxbMarshaller.marshal(this, file);
        } catch (JAXBException e) {
            ShowErrorDialog.showErrorDialog(e);
        }
    }
}
