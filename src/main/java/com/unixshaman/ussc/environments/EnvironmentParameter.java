package com.unixshaman.ussc.environments;

import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.XmlValue;

@XmlAccessorType()
@XmlType(name = "environmentparameter", propOrder = {"paramName", "paramValue"})
public class EnvironmentParameter {
    private String paramName;
    private String paramValue;

    public String getParamName() {
        return paramName;
    }

    @XmlAttribute
    public void setParamName(String paramName) {
        this.paramName = paramName;
    }

    public String getParamValue() {
        return paramValue;
    }

    @XmlValue
    public void setParamValue(String paramValue) {
        this.paramValue = paramValue;
    }
}
