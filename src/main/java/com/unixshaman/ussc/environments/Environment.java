package com.unixshaman.ussc.environments;

import com.unixshaman.ussc.ui.dialog.ShowErrorDialog;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import javax.xml.bind.annotation.*;
import java.io.File;
import java.util.ArrayList;
import java.util.List;


@XmlAccessorType()
@XmlRootElement(name = "environment")
@XmlType(name = "environment", propOrder = {"name", "pathToEnv", "parametersFiles"})
public class Environment {

    private String name;
    private String pathToEnv;
    private List<ParametersFile> parametersFiles = new ArrayList<>();


    public String getName() {
        return name;
    }

    @XmlAttribute
    public void setName(String name) {
        this.name = name;
    }

    public String getPathToEnv() {
        return pathToEnv;
    }

    @XmlAttribute
    public void setPathToEnv(String pathToEnv) {
        this.pathToEnv = pathToEnv;
    }

    public List<ParametersFile> getParametersFiles() {
        return parametersFiles;
    }

    @XmlElement(name = "parametersfile")
    public void setParametersFiles(List<ParametersFile> parametersFiles) {
        this.parametersFiles = parametersFiles;
    }

    public void addParametersFiles(List<ParametersFile> parametersFiles) {
        this.parametersFiles.addAll(parametersFiles);
    }

    public Environment() {

    }

    public Environment(String fileName) {
        this.pathToEnv = fileName;

        try {

            File file = new File(fileName);
            JAXBContext jaxbContext = JAXBContext.newInstance(Environment.class);

            Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();
            Environment environment = (Environment) jaxbUnmarshaller.unmarshal(file);

            this.name = environment.name;
            this.pathToEnv = environment.pathToEnv;
            this.parametersFiles = environment.parametersFiles;
        } catch (JAXBException e) {
            ShowErrorDialog.showErrorDialog(e);
        }
    }

    public void saveEnvironment() {
        saveEnvironment(this.pathToEnv);
    }

    public void saveEnvironment(String fileName) {
        try {
            File file = new File(fileName);
            JAXBContext jaxbContext = JAXBContext.newInstance(Environment.class);
            Marshaller jaxbMarshaller = jaxbContext.createMarshaller();

            jaxbMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);

            jaxbMarshaller.marshal(this, file);
        } catch (JAXBException e) {
            ShowErrorDialog.showErrorDialog(e);
        }
    }

}
