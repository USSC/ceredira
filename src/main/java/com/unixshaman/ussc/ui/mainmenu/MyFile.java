package com.unixshaman.ussc.ui.mainmenu;

import java.io.File;
import java.net.URI;

/**
 * Специальный класс, целью которого является переопределение
 * функции toString класса File, необходимо для вывода в
 * проводнике проекта не полного пути к файлу, а только его
 * имени
 */
public class MyFile extends File {

    public MyFile(String pathname) {
        super(pathname);
    }

    public MyFile(String parent, String child) {
        super(parent, child);
    }

    public MyFile(File parent, String child) {
        super(parent, child);
    }

    public MyFile(URI uri) {
        super(uri);
    }

    public MyFile(File file) {
        super(file.getAbsolutePath());
    }

    /**
     * Пришлось вручную создавать массив из объектов MyFile,
     * так как автоматически кастовать не получилось
     */
    @Override
    public MyFile[] listFiles() {
        File[] theFiles = super.listFiles();
        if (theFiles == null)
            return null;

        if (theFiles.length == 0)
            return new MyFile[0];

        MyFile[] myFiles = new MyFile[theFiles.length];
        for (int i = 0; i < theFiles.length; i++) {
            myFiles[i] = new MyFile(theFiles[i].getAbsolutePath());
        }
        return myFiles;
    }

    /**
     * Переопределение стандартного метода toString, для возврата только имени
     * файла/каталога
     * @return Имя каталога/файла
     */
    @Override
    public String toString() {
        return this.getName();
    }
}
