package com.unixshaman.ussc.ui.mainmenu;

import com.unixshaman.ussc.ui.common.common;
import javafx.scene.control.ContextMenu;
import javafx.scene.control.MenuItem;
import javafx.scene.control.cell.TextFieldTreeCell;

/**
 * Кастомный пункт дерева проекта, для создания контекстного меню
 * у пунктов, в зависимости от их сущности
 */
public class MyTreeCell extends TextFieldTreeCell<MyFile> {
    private ContextMenu rootContextMenu;
    private ContextMenu operationsRootContextMenu;
    private ContextMenu operationsContextMenu;
    private ContextMenu environmentsRootContextMenu;
    private ContextMenu environmentsContextMenu;
    private ContextMenu testsRootContextMenu;
    private ContextMenu testsContextMenu;


    public MyTreeCell() {
        // TODO: Добавить пункт Копировать путь для каждого типа пункта
        // TODO: Добавить пункт Копировать имя для каждого типа пункта

        // Создание меню для корневого пункта проекта
        // TODO: Добавить пункт Открыть в проводнике
        // TODO: Добавить пункт Новый -> {Каждая сущность}
        // TODO: УБрать мусор
        rootContextMenu = new ContextMenu();
        MenuItem mi1 = new MenuItem("Обновить");
        mi1.setOnAction(event -> common.refreshProjectTree());
        rootContextMenu.getItems().add(mi1);

        // Контекстное меню, для сущности Операции (каталог)
        operationsRootContextMenu = new ContextMenu();
        MenuItem mi2 = new MenuItem("Создать новую группу операций");
        mi2.setOnAction(event -> common.createNewOperationGroup());
        operationsRootContextMenu.getItems().add(mi2);

        // Контекстное меню, для сущности Операция (файл)
        operationsContextMenu = new ContextMenu();
        MenuItem mi3 = new MenuItem("Открыть группу операций");
        mi3.setOnAction(event -> common.openOperationsGroup(this.getItem().getAbsolutePath()));
        operationsContextMenu.getItems().add(mi3);

        // Контекстное меню, для сущности Окружение (каталог)
        environmentsRootContextMenu = new ContextMenu();
        MenuItem mi4 = new MenuItem("Создать новое окружение");
        mi4.setOnAction(event -> common.createNewEnvironment());
        environmentsRootContextMenu.getItems().add(mi4);

        // Контекстное меню, для сущности Окружение (файл)
        environmentsContextMenu = new ContextMenu();
        MenuItem mi5 = new MenuItem("Открыть окружение");
        mi5.setOnAction(event -> common.openEnvironment(this.getItem().getAbsolutePath()));
        environmentsContextMenu.getItems().add(mi5);

        // Контекстное меню, для сущности Тест (каталог)
        testsRootContextMenu = new ContextMenu();
        MenuItem mi6 = new MenuItem("Создать новый тест");
        mi6.setOnAction(event -> common.createNewTest());
        testsRootContextMenu.getItems().add(mi6);

        // Контекстное меню, для сущности Тест (файл)
        testsContextMenu = new ContextMenu();
        MenuItem mi7 = new MenuItem("Открыть тест");
        mi7.setOnAction(event -> common.openTest(this.getItem().getAbsolutePath()));
        testsContextMenu.getItems().add(mi7);
    }

    @Override
    public void updateItem(MyFile item, boolean empty) {
        super.updateItem(item, empty);

        // Назначение определенным пунктам дерева, соответствующих контекстных меню
        if (!empty && getTreeItem().getParent() == null) {
            setContextMenu(rootContextMenu);
        } else if (!empty && getTreeItem().getValue().getName().equals("Operations")) {
            setContextMenu(operationsRootContextMenu);
        } else if (!empty && getTreeItem().getValue().getName().endsWith(".grop")) {
            setContextMenu(operationsContextMenu);
        } else if (!empty && getTreeItem().getValue().getName().equals("Environments")) {
            setContextMenu(environmentsRootContextMenu);
        } else if (!empty && getTreeItem().getParent().toString().contains("Environments")) {
            setContextMenu(environmentsContextMenu);
        } else if (!empty && getTreeItem().getValue().getName().equals("Tests")) {
            setContextMenu(testsRootContextMenu);
        } else if (!empty && getTreeItem().getValue().getName().endsWith(".test")) {
            setContextMenu(testsContextMenu);
        }
    }
}
