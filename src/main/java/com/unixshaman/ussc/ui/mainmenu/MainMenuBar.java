package com.unixshaman.ussc.ui.mainmenu;

import com.unixshaman.ussc.Ceredira;
import com.unixshaman.ussc.ui.common.common;
import com.unixshaman.ussc.ui.dialog.CreateNewProjectWindow;
import javafx.scene.control.*;
import javafx.stage.DirectoryChooser;

import java.io.File;
import java.util.Arrays;

import static com.unixshaman.ussc.ui.common.common.initProjectExplorer;

/**
 * Основное меню приложения
 */
public class MainMenuBar extends MenuBar {

    private Ceredira application;

    /**
     * Получить основное меню приложения
     * @param application
     * @return Основное меню приложения
     */
    public MainMenuBar(Ceredira application)
    {
        this.application = application;

        Menu fileMenu = new Menu("Файл");

        MenuItem createProjectMenuItem = new MenuItem("Создать проект...");
        createProjectMenuItem.setOnAction(event -> createProject());
        fileMenu.getItems().add(createProjectMenuItem);

        MenuItem openProjectMenuItem = new MenuItem("Открыть проект...");
        openProjectMenuItem.setOnAction(event -> openProject());
        fileMenu.getItems().add(openProjectMenuItem);

        MenuItem closeProjectMenuItem = new MenuItem("Закрыть проект...");
        closeProjectMenuItem.setOnAction(event -> closeProject());
        fileMenu.getItems().add(closeProjectMenuItem);

        fileMenu.getItems().add(new SeparatorMenuItem());

        MenuItem createOperationGroupMenuItem = new MenuItem("Создать новую группу операций...");
        createOperationGroupMenuItem.setOnAction(e -> common.createNewOperationGroup());
        fileMenu.getItems().add(createOperationGroupMenuItem);


        MenuItem createEnvironmentMenuItem = new MenuItem("Создать новое окружение...");
        createEnvironmentMenuItem.setOnAction(e -> common.createNewEnvironment());
        fileMenu.getItems().add(createEnvironmentMenuItem);

        MenuItem createTestMenuItem = new MenuItem("Создать новый тест...");
        createTestMenuItem.setOnAction(e -> common.createNewTest());
        fileMenu.getItems().add(createTestMenuItem);

        fileMenu.getItems().add(new SeparatorMenuItem());

        MenuItem exitMenuItem = new MenuItem("Выход");
        exitMenuItem.setOnAction(e -> application.getMainStage().close());
        fileMenu.getItems().add(exitMenuItem);

        Menu viewMenu = new Menu("Вид");

        CheckMenuItem projectView = new CheckMenuItem("Проект");

        this.getMenus().addAll(fileMenu, viewMenu);
    }

    private void openProject() {
        // Диалог открытия проекта
        DirectoryChooser directoryChooser = new DirectoryChooser();
        File selectedDirectory = directoryChooser.showDialog(application.getMainStage());

        if(selectedDirectory != null) {
            application.getMainLeftPane().getProjectExplorerTab().setContent(initProjectExplorer(new MyFile(selectedDirectory.getAbsolutePath())));
            application.getApplicationContext().setProjectPath(selectedDirectory.toPath());
        }
    }

    private void createProject() {
        MyFile selectedDirectory = (new CreateNewProjectWindow()).display();

        if (selectedDirectory != null) {
            for (String folder: Arrays.asList("DataPools", "Environments", "Operations", "Reports", "Steps", "Suites", "Tests", "Tools")) {
                (new MyFile(selectedDirectory, folder)).mkdirs();
            }

            application.getMainLeftPane().getProjectExplorerTab().setContent(initProjectExplorer(selectedDirectory));
            application.getApplicationContext().setProjectPath(selectedDirectory.toPath());
            application.getApplicationContext().setProjectOpened(true);
        }
    }

    private void closeProject() {
        application.getMainLeftPane().getProjectExplorerTab().setContent(null);
        application.getApplicationContext().setProjectPath(null);
        application.getApplicationContext().setProjectOpened(false);
    }


}
