package com.unixshaman.ussc.ui.customcontrols;

import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.TextArea;
import javafx.scene.layout.StackPane;

public class RichTextArea extends StackPane {

    private Button decreaseParameterValueHeight;

    private Button increaseParameterValueHeight;

    private TextArea textArea;

    private Integer minHeight = 25;

    private Integer prefHeight = 25;

    private Integer maxHeight = 2500;

    private String text = "";

    private String promptText = "";

    public TextArea getTextArea() {
        return textArea;
    }

    public String getText() {
        return textArea.getText();
    }

    public void setText(String value) {
        textArea.setText(value);
    }

    public RichTextArea() {
        initRichTextArea();
    }

    public RichTextArea(String text, String promptText) {
        this.text = text;
        this.promptText = promptText;
        initRichTextArea();
    }

    public RichTextArea(String text, String promptText, Integer prefHeight) {
        this.text = text;
        this.promptText = promptText;
        this.prefHeight = prefHeight;
        initRichTextArea();
    }

    public RichTextArea(String text, String promptText, Integer minHeight, Integer maxHeight) {
        this.text = text;
        this.promptText = promptText;
        this.minHeight = minHeight;
        this.maxHeight = maxHeight;
        initRichTextArea();
    }

    private void initRichTextArea() {
        textArea = new TextArea();
        textArea.setId("RichTextArea");
        textArea.setMaxHeight(maxHeight);
        textArea.setMinHeight(minHeight);
        textArea.setPrefHeight(prefHeight);
        textArea.setText(text);
        textArea.setPromptText(promptText);

        decreaseParameterValueHeight = new Button("-");
        decreaseParameterValueHeight.setOpacity(0.5);
        decreaseParameterValueHeight.setOnAction(event -> {
            if (textArea.getPrefHeight() > minHeight) {
                textArea.setPrefHeight(textArea.getPrefHeight() / 2);
            }
        });

        increaseParameterValueHeight = new Button("+");
        increaseParameterValueHeight.setOpacity(0.5);
        increaseParameterValueHeight.setOnAction(event -> {
            if (textArea.getPrefHeight() < maxHeight) {
                textArea.setPrefHeight(textArea.getPrefHeight() * 2);
            }
        });

        StackPane.setMargin(increaseParameterValueHeight, new Insets(0, 40, 0, 0));
        StackPane.setMargin(decreaseParameterValueHeight, new Insets(0, 20, 0, 0));
        StackPane.setAlignment(decreaseParameterValueHeight, Pos.TOP_RIGHT);
        StackPane.setAlignment(increaseParameterValueHeight, Pos.TOP_RIGHT);

        this.getChildren().addAll(textArea, decreaseParameterValueHeight, increaseParameterValueHeight);
    }
}
