package com.unixshaman.ussc.ui.editor;

import com.unixshaman.ussc.ApplicationContext;
import com.unixshaman.ussc.Ceredira;
import com.unixshaman.ussc.operations.Operation;
import com.unixshaman.ussc.operations.OperationGroup;
import com.unixshaman.ussc.operations.Parameter;
import com.unixshaman.ussc.ui.customcontrols.RichTextArea;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.control.*;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.util.StringConverter;

import javax.script.ScriptContext;
import javax.script.ScriptEngine;
import javax.script.ScriptEngineManager;
import javax.script.SimpleBindings;
import java.util.Arrays;

import static com.unixshaman.ussc.ui.common.common.*;

/**
 * Created by gdjamalov on 18.07.2017.
 */
public class OperationGroupsEditor extends Tab {

    Ceredira application;

    private TextField groupNameTextField;
    private TextField groupTitleTextField;
    private TextField fileNameTextField;
    private RichTextArea groupDescriptionTextArea;

    private VBox operationsList;

    public OperationGroupsEditor(Ceredira application) {
        this.application = application;

        operationsList = new VBox(5);

        Label groupNameLabel = new Label("Имя группы:");
        Label groupTitleLabel = new Label("Заголовок группы:");
        Label fileNameLabel = new Label("Путь к файлу:");
        Label groupDescriptionLabel = new Label("Описание группы");

        groupNameTextField = new TextField();
        groupNameTextField.setId("groupNameTextField");

        groupTitleTextField = new TextField();
        groupTitleTextField.setId("groupTitleTextField");
        groupTitleTextField.setText("Новая группа");

        fileNameTextField = new TextField();
        fileNameTextField.setId("fileNameTextField");
        fileNameTextField.textProperty().bindBidirectional(groupTitleTextField.textProperty(), new StringConverter<String>() {
            @Override
            public String toString(String object) {
                return ApplicationContext.getInstance().getOperationsPath() + "\\" + groupTitleTextField.getText() + ".grop";
            }

            @Override
            public String fromString(String string) {
                return string.replace(ApplicationContext.getInstance().getOperationsPath() + "\\", "").replace(".grop", "");
            }
        });

        groupDescriptionTextArea = new RichTextArea("", "", 75);
        groupDescriptionTextArea.setId("groupDescriptionTextArea");

        groupNameTextField.setOnAction(event -> {
            if (fileNameTextField.getText().equals("")) {
                //fileNameTextField.setText(((Node)event.getTarget()).getScene().getWindow().);
            }
        });

        Button saveGroup = new Button("Сохранить");
        saveGroup.setOnAction(event -> saveOperationGroup());

        Button saveAsGroup = new Button("Сохранить как...");

        HBox hb1 = new HBox(20, saveGroup, saveAsGroup);
        hb1.setAlignment(Pos.CENTER);

        Button addOperationAtTheStart = new Button("Добавить новую операцию в начале");
        addOperationAtTheStart.setOnAction(event -> operationsList.getChildren().add(0, new OperationBox()));

        Button addOperationAtTheEnd = new Button("Добавить новую операцию в конце");
        addOperationAtTheEnd.setOnAction(event -> operationsList.getChildren().add(new OperationBox()));

        Button expandAllOperations = new Button("Развернуть все операции");
        expandAllOperations.setOnAction(event -> setExpandedForAll(operationsList.getChildren(), true));

        Button collapseAllOperations = new Button("Свернуть все операции");
        collapseAllOperations.setOnAction(event -> setExpandedForAll(operationsList.getChildren(), false));

        HBox hb2 = new HBox(20, addOperationAtTheStart, addOperationAtTheEnd, expandAllOperations, collapseAllOperations);
        hb2.setAlignment(Pos.CENTER);

        GridPane gp1 = new GridPane();
        gp1.setPadding(new Insets(10, 10, 10,10 ));
        gp1.setHgap(8);
        gp1.setVgap(8);
        setColumnsPercentage(gp1, 15, 70, 15);

        gp1.add(groupNameLabel, 0, 0, 1, 1);
        gp1.add(groupNameTextField, 1, 0, 2, 1);
        gp1.add(groupTitleLabel, 0, 1, 1, 1);
        gp1.add(groupTitleTextField, 1, 1, 2, 1);
        gp1.add(fileNameLabel, 0, 2, 1, 1);
        gp1.add(fileNameTextField, 1, 2, 1, 1);
        gp1.add(hb1, 2, 2);
        gp1.add(groupDescriptionLabel, 0, 3, 1, 1);
        gp1.add(groupDescriptionTextArea, 1, 3, 2, 1);
        gp1.add(hb2, 0, 4, 3, 1);

        ScrollPane sp = new ScrollPane(operationsList);
        VBox layout = new VBox(20, gp1, sp);

        operationsList.prefWidthProperty().bind(sp.widthProperty().subtract(15));

        this.setContent(layout);
        this.setClosable(true);
        this.textProperty().bind(groupTitleTextField.textProperty());
    }

    public OperationGroupsEditor(Ceredira application, String pathToOperationsGroup) {
        this(application);

        OperationGroup operationGroup = new OperationGroup(pathToOperationsGroup);

        groupNameTextField.setText(operationGroup.getName());
        groupTitleTextField.setText(operationGroup.getTitle());
        fileNameTextField.setText(operationGroup.getFileName());
        groupDescriptionTextArea.setText(operationGroup.getDescription());

        // Цикл по операциям
        for (Operation op : operationGroup.getOperations()) {

            OperationBox operation = new OperationBox();

            operation.getOperationNameTextField().setText(op.getName());
            operation.getOperationTitleTextField().setText(op.getTitle());
            operation.getOperationShortDescriptionTextField().setText(op.getShortDescription());
            operation.getOperationDetailedDescriptionTextArea().setText(op.getDetailedDescription());
            operation.getOperationParametersTextArea().setText(op.getExampleParameters());
            operation.getOperationCodeTextArea().setText(op.getCode());

            operationsList.getChildren().add(operation);
        }
    }

    private void saveOperationGroup() {
        OperationGroup operationGroup = new OperationGroup();
        operationGroup.setName(groupNameTextField.getText());
        operationGroup.setTitle(groupTitleTextField.getText());
        operationGroup.setFileName(fileNameTextField.getText());
        operationGroup.setDescription(groupDescriptionTextArea.getText());

        // Цикл по операциям в группе
        for (Node node: operationsList.getChildren()) {
            if (node instanceof OperationBox) {
                Operation op = new Operation();
                op.setName(((OperationBox) node).getOperationNameTextField().getText());
                op.setTitle(((OperationBox) node).getOperationTitleTextField().getText());
                op.setShortDescription(((OperationBox) node).getOperationShortDescriptionTextField().getText());
                op.setDetailedDescription(((OperationBox) node).getOperationDetailedDescriptionTextArea().getText());
                op.setExampleParameters(((OperationBox) node).getOperationParametersTextArea().getText());
                op.setCode(((OperationBox) node).getOperationCodeTextArea().getText());

                String operationParameters = ((OperationBox)node).getOperationParametersTextArea().getText();

                // Цикл по параметрам
                if (operationParameters.contains("\n###"))
                {
                    for (String iteration : operationParameters.split("\n###")) {
                        parseParametersForScript(op, iteration);
                    }
                } else {
                    parseParametersForScript(op, operationParameters);
                }
                operationGroup.addOperations(Arrays.asList(op));
            }
        }

        operationGroup.saveGroup();
    }

    private void parseParametersForScript(Operation op, String parameters) {
        for (String param : parameters.split("\n")) {
            if (param.split(":", 2).length > 1) {
                Parameter prm = new Parameter();
                prm.setName(param.split(":")[0].trim());
                op.addParameters(Arrays.asList(prm));
            }
        }
    }

    class OperationBox extends TitledPane {

        private TextField operationNameTextField;

        private TextField operationTitleTextField;

        private TextField operationShortDescriptionTextField;

        private RichTextArea operationDetailedDescriptionTextArea;

        private RichTextArea operationParametersTextArea;

        private RichTextArea operationCodeTextArea;

        private RichTextArea operationDebugWindowTextArea;

        public OperationBox() {

            HBox operationTitle = new HBox(10);

            Button deleteOperation = new Button("Удалить");
            deleteOperation.setUnderline(true);
            deleteOperation.setOnAction(event -> operationsList.getChildren().remove(this));

            Button upOperation = new Button("Выше");
            upOperation.setUnderline(true);
            upOperation.setOnAction(event -> moveObjectUp(operationsList.getChildren(), this));

            Button downOperation = new Button("Ниже");
            downOperation.setUnderline(true);
            downOperation.setOnAction(event -> moveObjectDown(operationsList.getChildren(), this));

            operationTitle.getChildren().addAll(upOperation, downOperation, deleteOperation);

            this.setGraphic(operationTitle);

            GridPane gp = new GridPane();
            gp.setHgap(8);
            gp.setVgap(8);

            Label operationNameLabel = new Label("Имя операции:");
            Label operationTitleLabel = new Label("Заголовок операции:");
            Label operationShortDescriptionLabel = new Label("Краткое описание операции:");
            Label operationDetailedDescriptionLabel = new Label("Полное описание операции:");
            Label operationParametersLabel = new Label("Входные параметры операции:");
            Label operationCodeDescriptionLabel = new Label("Исходный код операции:");
            Label operationDebugWindowLabel = new Label("Результат работы операции:");

            operationNameTextField = new TextField();
            operationNameTextField.setId("operationNameTextField");
            operationTitleTextField = new TextField();
            operationTitleTextField.setId("operationTitleTextField");
            operationShortDescriptionTextField = new TextField();
            operationShortDescriptionTextField.setId("operationShortDescriptionTextField");
            operationDetailedDescriptionTextArea = new RichTextArea("", "", 75);
            operationDetailedDescriptionTextArea.setId("operationDetailedDescriptionTextArea");
            operationParametersTextArea = new RichTextArea("", "", 100);
            operationParametersTextArea.setId("operationParametersTextArea");
            operationCodeTextArea = new RichTextArea("", "", 100);
            operationCodeTextArea.setId("operationCodeTextArea");
            operationDebugWindowTextArea = new RichTextArea("", "", 100);

            operationTitleTextField.textProperty().addListener((observable, oldValue, newValue) -> {
                this.setText(newValue);
            });

            setColumnsPercentage(gp, 25, 75);

            Button executeButton = new Button("Запустить");
            executeButton.setOnAction(event -> {
                if (operationParametersTextArea.getText().contains("\n###")) {
                    for (String iteration : operationParametersTextArea.getText().split("\n###")) {
                        operationDebugWindowTextArea.setText("###\n" + executeJavaScriptCode(iteration, operationCodeTextArea.getText()) + "\n\n\n");
                    }
                } else {
                    operationDebugWindowTextArea.setText(executeJavaScriptCode(operationParametersTextArea.getText(), operationCodeTextArea.getText()));
                }
            });

            gp.add(operationNameLabel, 0, 0);
            gp.add(operationNameTextField, 1, 0);
            gp.add(operationTitleLabel, 0, 1);
            gp.add(operationTitleTextField, 1, 1);
            gp.add(operationShortDescriptionLabel, 0, 2);
            gp.add(operationShortDescriptionTextField, 1, 2);
            gp.add(operationDetailedDescriptionLabel, 0, 3, 2,1);
            gp.add(operationDetailedDescriptionTextArea,0, 4, 2,1);
            gp.add(operationParametersLabel, 0, 5, 2,1);
            gp.add(operationParametersTextArea, 0, 6, 2,1);
            gp.add(operationCodeDescriptionLabel, 0, 7, 2,1);
            gp.add(operationCodeTextArea, 0, 8, 2,1);
            gp.add(executeButton, 0, 9, 2,1);
            gp.add(operationDebugWindowLabel, 0, 10, 2,1);
            gp.add(operationDebugWindowTextArea, 0, 11, 2,1);

            this.setContent(gp);
            this.setText(operationTitleTextField.getText());
        }

        private String executeJavaScriptCode(String parameters, String code) {
            try {
                ScriptEngine engine = new ScriptEngineManager().getEngineByName("JavaScript");
                SimpleBindings bindings = new SimpleBindings();
                engine.setBindings(bindings, ScriptContext.ENGINE_SCOPE);
                for (String param : parameters.split("\n")) {
                    if (!param.equals("") && param.split(":", 1).length >= 1)
                        engine.put(param.split(":", 2)[0].trim(), param.split(":", 2)[1].trim());
                }
                return (String) engine.eval(code);
            } catch (Exception ex) {
                return ex.getMessage();
            }
        }

        public TextField getOperationNameTextField() {
            return operationNameTextField;
        }

        public TextField getOperationTitleTextField() {
            return operationTitleTextField;
        }

        public TextField getOperationShortDescriptionTextField() {
            return operationShortDescriptionTextField;
        }

        public RichTextArea getOperationDetailedDescriptionTextArea() {
            return operationDetailedDescriptionTextArea;
        }

        public RichTextArea getOperationParametersTextArea() {
            return operationParametersTextArea;
        }

        public RichTextArea getOperationCodeTextArea() {
            return operationCodeTextArea;
        }

        public RichTextArea getOperationDebugWindowTextArea() {
            return operationDebugWindowTextArea;
        }
    }
}