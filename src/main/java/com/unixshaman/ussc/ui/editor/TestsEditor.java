package com.unixshaman.ussc.ui.editor;

import com.unixshaman.ussc.ui.customcontrols.RichTextArea;
import javafx.geometry.Insets;
import javafx.scene.control.*;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;

import static com.unixshaman.ussc.ui.common.common.*;


public class TestsEditor {

    public Tab crateNewTestsEditor() {
        return display("");
    }

    public Tab openTestsEditor(String pathToTest) {
        return display(pathToTest);
    }

    private Tab display(String pathToTest) {
        Tab tab = new Tab();
        tab.setClosable(true);

        GridPane gp1 = new GridPane();
        gp1.setPadding(new Insets(10, 10, 10,10 ));
        gp1.setHgap(8);
        gp1.setVgap(8);

        Label testNameLabel = new Label("Имя теста:");
        Label testAuthorLabel = new Label("Автор теста:");
        Label testVersionLabel = new Label("Версия теста:");
        Label testDescriptionLabel = new Label("Описание теста:");

        TextField testNameTextField = new TextField();
        TextField testAuthorTextField = new TextField("");
        TextField testVersionTextField = new TextField();
        RichTextArea testDescriptionTextArea = new RichTextArea("", "", 75);

        Button saveTestButton = new Button("Сохранить");
        Button saveAsTestButton = new Button("Сохранить как");

        HBox hb1 = new HBox(10, testNameTextField, saveTestButton, saveAsTestButton);
        setColumnsPercentage(gp1, 15, 85);
        gp1.add(testNameLabel, 0, 0);
        gp1.add(hb1, 1, 0);
        gp1.add(testAuthorLabel, 0, 1);
        gp1.add(testAuthorTextField, 1, 1);
        gp1.add(testVersionLabel, 0, 2);
        gp1.add(testVersionTextField, 1, 2);
        gp1.add(testDescriptionLabel, 0, 3);
        gp1.add(testDescriptionTextArea, 1, 3);


        GridPane testStepsGrid = new GridPane();
        setColumnsPercentage(testStepsGrid, 5, 10, 15, 30, 20, 20);

        TitledPane testStepsTitledPane = new TitledPane("Шаги теста", testStepsGrid);

        VBox vb2 = new VBox(20, testParametersTitledPane(), testStepsTitledPane);

        ScrollPane sp = new ScrollPane(vb2);
        vb2.prefWidthProperty().bind(sp.widthProperty().subtract(15));

        VBox layout = new VBox(20, gp1, sp);

        tab.setContent(layout);

        return tab;
    }

    private TitledPane testParametersTitledPane() {
        GridPane testParametersGrid = new GridPane();
        testParametersGrid.setHgap(8);
        testParametersGrid.setVgap(8);
        setColumnsPercentage(testParametersGrid, 5, 25, 30, 20, 20);



        TitledPane testParametersTitledPane = new TitledPane("Параметры теста", testParametersGrid);

        VBox testParametersVBox = new VBox(2, testParameterBox());

        Button addTestParameter = new Button("Добавить");
        addTestParameter.setOnAction(event -> testParametersVBox.getChildren().add(testParameterBox()));

        testParametersTitledPane.setGraphic(addTestParameter);

        testParametersTitledPane.setContent(testParametersVBox);

        return testParametersTitledPane;
    }

    private GridPane testParameterBox() {
        GridPane gp = new GridPane();
        gp.setHgap(8);
        gp.setVgap(8);

        Button addTestParameterButton = new Button("+");
        Button deleteTestParameterButton = new Button("-");
        Button moveUpTestParameter = new Button("<");
        Button moveDownTestParameter = new Button(">");

        HBox controlButtons = new HBox(0, addTestParameterButton, deleteTestParameterButton, moveUpTestParameter, moveDownTestParameter);

        addTestParameterButton.setOnAction(event -> {
            insertAfter(((VBox)gp.getParent()).getChildren(), gp, testParameterBox());
        });

        deleteTestParameterButton.setOnAction(event -> {
            ((VBox)gp.getParent()).getChildren().remove(gp);
        });

        moveUpTestParameter.setOnAction(event -> {
            moveObjectUp(((VBox)gp.getParent()).getChildren(), gp);
        });

        moveDownTestParameter.setOnAction(event -> {
            moveObjectDown(((VBox)gp.getParent()).getChildren(), gp);
        });


        setColumnsPercentage(gp, 8, 27, 65);


        TextField testParameterNameValue = new TextField();
        testParameterNameValue.setPromptText("Имя параметра");
        RichTextArea testParameterValue = new RichTextArea("", "Значение параметра");


        gp.add(controlButtons, 0, 0);
        gp.add(testParameterNameValue, 1, 0);
        gp.add(testParameterValue, 2, 0);


        return gp;

    }

}
