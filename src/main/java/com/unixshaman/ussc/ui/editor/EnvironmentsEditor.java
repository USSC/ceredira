package com.unixshaman.ussc.ui.editor;

import com.unixshaman.ussc.Ceredira;
import com.unixshaman.ussc.environments.EnvironmentParameter;
import com.unixshaman.ussc.environments.ParametersFile;
import com.unixshaman.ussc.ui.common.common;
import com.unixshaman.ussc.ui.customcontrols.RichTextArea;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.control.*;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.util.StringConverter;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Arrays;

import static com.unixshaman.ussc.ui.common.common.*;

public class EnvironmentsEditor extends Tab {

    private Ceredira application;

    private VBox layout;
    
    private VBox environmentFileVBox;

    private TextField environmentNameTextField;

    private TextField environmentPathTextField;

    public EnvironmentsEditor(Ceredira application) {
        this.application = application;

        this.setClosable(true);

        layout = new VBox(20);
        ScrollPane environmentFileScrollPane = new ScrollPane();
        environmentFileVBox = new VBox(5);

        GridPane environmentEditorHeaderGridPane = new GridPane();
        environmentEditorHeaderGridPane.setPadding(new Insets(10, 10, 10,10 ));
        environmentEditorHeaderGridPane.setHgap(8);
        environmentEditorHeaderGridPane.setVgap(8);

        setColumnsPercentage(environmentEditorHeaderGridPane, 15, 70, 15);

        Label environmentNameLabel = new Label("Имя окружения: ");
        Label environmentPathLabel = new Label("Путь к окружению: ");

        environmentNameTextField = new TextField();
        environmentNameTextField.setId("environmentNameTextField");
        environmentPathTextField = new TextField();
        environmentPathTextField.setId("environmentPathTextField");

        environmentPathTextField.textProperty().bindBidirectional(environmentNameTextField.textProperty(), new StringConverter<String>() {
            @Override
            public String toString(String object) {
                return application.getApplicationContext().getEnvironmentsPath() + "\\" + environmentNameTextField.getText();
            }

            @Override
            public String fromString(String string) {
                return string.replace(application.getApplicationContext().getEnvironmentsPath() + "\\", "");
            }
        });
        this.textProperty().bind(environmentNameTextField.textProperty());

        environmentNameTextField.setText("Новое окружение");

        Button saveEnvironment = new Button("Сохранить");
        Button saveAsEnvironment = new Button("Сохранить как...");

        saveEnvironment.setOnAction(event -> {
            for (Node node: environmentFileVBox.getChildren()) {
                if (node instanceof ParametersFileTitledPane) {
                    ParametersFile pf = new ParametersFile();
                    pf.setPathToFile(Paths.get(this.environmentPathTextField.getText(), ((ParametersFileTitledPane) node).getPathToEnvFileTextField().getText()).toString());

                    for (Node param: ((ParametersFileTitledPane) node).getParametersList().getChildren()) {
                        if (param instanceof ParametersFileTitledPane.ParameterItemGridPane) {
                            EnvironmentParameter ep = new EnvironmentParameter();
                            ep.setParamName(((ParametersFileTitledPane.ParameterItemGridPane) param).getParameterNameTextField().getText());
                            ep.setParamValue(((ParametersFileTitledPane.ParameterItemGridPane) param).getParameterValueTextField().getText());

                            pf.addEnvironmentParameters(Arrays.asList(ep));
                        }
                    }
                    pf.saveParametersFile();
                }
            }
            common.refreshProjectTree();
        });

        HBox hb1 = new HBox(20);
        hb1.setAlignment(Pos.CENTER_LEFT);
        HBox hb2 = new HBox(20);
        hb2.setAlignment(Pos.CENTER);

        Button addEnvironmentAtTheStart = new Button("Добавить новый файл с параметрами в начало");
        addEnvironmentAtTheStart.setOnAction(event -> environmentFileVBox.getChildren().add(0, new ParametersFileTitledPane()));

        Button addEnvironmentAtTheEnd = new Button("Добавить новый файл с параметрами в конец");
        addEnvironmentAtTheEnd.setOnAction(event -> environmentFileVBox.getChildren().add(new ParametersFileTitledPane()));

        Button expandAllEnvironments = new Button("Развернуть все файлы с параметрами");
        expandAllEnvironments.setOnAction(event -> setExpandedForAll(environmentFileVBox.getChildren(), true));

        Button collapseAllEnvironments = new Button("Свернуть все файлы с параметрами");
        collapseAllEnvironments.setOnAction(event -> setExpandedForAll(environmentFileVBox.getChildren(), false));

        hb1.getChildren().addAll(saveEnvironment, saveAsEnvironment);
        hb2.getChildren().addAll(addEnvironmentAtTheStart, addEnvironmentAtTheEnd, expandAllEnvironments, collapseAllEnvironments);

        environmentEditorHeaderGridPane.add(environmentNameLabel, 0, 0, 1, 1);
        environmentEditorHeaderGridPane.add(environmentNameTextField,1, 0, 2, 1);
        environmentEditorHeaderGridPane.add(environmentPathLabel, 0, 1, 1, 1);
        environmentEditorHeaderGridPane.add(environmentPathTextField, 1, 1, 1, 1);
        environmentEditorHeaderGridPane.add(hb1, 2, 1);
        environmentEditorHeaderGridPane.add(hb2, 0, 2, 3, 1);
        
        environmentFileVBox.prefWidthProperty().bind(environmentFileScrollPane.widthProperty().subtract(15));

        environmentFileScrollPane.setContent(environmentFileVBox);

        layout.getChildren().addAll(environmentEditorHeaderGridPane, environmentFileScrollPane);

        this.setContent(layout);
    }

    public EnvironmentsEditor(Ceredira application, String pathToEnvironment) {
        this(application);

        try {
            this.environmentPathTextField.setText(pathToEnvironment);
            Files.walk(Paths.get(pathToEnvironment))
                    .filter(p -> p.toString().endsWith(".env"))
                    .distinct()
                    .forEach(path -> {
                        ParametersFile pf = new ParametersFile(path.toString());
                        ParametersFileTitledPane pftp = new ParametersFileTitledPane();
                        pftp.getPathToEnvFileTextField().setText(pf.getPathToFile().replace(this.environmentPathTextField.getText(), ""));
                        for (EnvironmentParameter ep :pf.getEnvironmentParameters()) {
                            pftp.addParameterItem(ep.getParamName(), ep.getParamValue());
                        }
                        this.environmentFileVBox.getChildren().add(pftp);
                    });
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    class ParametersFileTitledPane extends TitledPane {

        private VBox parametersList;

        private TextField pathToEnvFileTextField;

        public ParametersFileTitledPane() {
            this.setId("parametersFileTitledPane");
            this.setPadding(new Insets(10, 10, 10, 10));

            parametersList = new VBox(10);
            parametersList.setId("parametersList");
            parametersList.setPadding(new Insets(10, 10, 10, 10));

            HBox hbTitle = new HBox(10);
            Button deleteEnvironment = new Button("Удалить");
            deleteEnvironment.setUnderline(true);
            deleteEnvironment.setOnAction(event -> environmentFileVBox.getChildren().remove(this));

            Button upEnvironment = new Button("Выше");
            upEnvironment.setUnderline(true);
            upEnvironment.setOnAction(event -> moveObjectUp(environmentFileVBox.getChildren(), this));

            Button downEnvironment = new Button("Ниже");
            downEnvironment.setUnderline(true);
            downEnvironment.setOnAction(event -> moveObjectDown(environmentFileVBox.getChildren(), this));

            Button addNewParameter = new Button("Добавить параметр");
            addNewParameter.setOnAction(event -> parametersList.getChildren().add(new ParameterItemGridPane()));

            hbTitle.getChildren().addAll(upEnvironment, downEnvironment, deleteEnvironment, addNewParameter);

            this.setGraphic(hbTitle);

            Label pathToEnvFileLabel = new Label("Путь к файлу:");
            pathToEnvFileTextField = new TextField();
            pathToEnvFileTextField.setId("pathToEnvFileTextField");
            pathToEnvFileTextField.setPromptText("\\folder\\file.env");
            pathToEnvFileTextField.setMaxWidth(Double.MAX_VALUE);

            GridPane fileHeader = new GridPane();
            fileHeader.setId("fileHeader");
            fileHeader.setVgap(8);
            fileHeader.setHgap(8);
            setColumnsPercentage(fileHeader, 10, 90);
            fileHeader.add(pathToEnvFileLabel, 0, 0);
            fileHeader.add(pathToEnvFileTextField, 1, 0);

            this.textProperty().bind(pathToEnvFileTextField.textProperty());

            parametersList.getChildren().addAll(fileHeader);

            this.setContent(parametersList);
        }

        public VBox getParametersList() {
            return parametersList;
        }

        public TextField getPathToEnvFileTextField() {
            return pathToEnvFileTextField;
        }

        public void addParameterItem(String parameterName, String parameterValue) {
            ParameterItemGridPane pigp = new ParameterItemGridPane();
            pigp.parameterNameTextField.setText(parameterName);
            pigp.parameterValueTextField.setText(parameterValue);
            parametersList.getChildren().add(pigp);
        }

        class ParameterItemGridPane extends GridPane {

            private TextField parameterNameTextField;

            private RichTextArea parameterValueTextField;

            public ParameterItemGridPane() {
                this.setHgap(8);
                this.setVgap(8);

                Button addParameterButton = new Button("+");
                Button deleteParameterButton = new Button("-");
                Button moveUpParameter = new Button("<");
                Button moveDownParameter = new Button(">");

                HBox controlButtons = new HBox(0, addParameterButton, deleteParameterButton, moveUpParameter, moveDownParameter);

                addParameterButton.setOnAction(event -> insertAfter(parametersList.getChildren(), this, new ParameterItemGridPane()));

                deleteParameterButton.setOnAction(event -> parametersList.getChildren().remove(this));

                moveUpParameter.setOnAction(event -> moveObjectUp(parametersList.getChildren(), this));

                moveDownParameter.setOnAction(event -> moveObjectDown(parametersList.getChildren(), this));

                parameterNameTextField = new TextField();
                parameterNameTextField.setPromptText("Имя параметра");

                parameterValueTextField = new RichTextArea("", "Значение параметра");

                setColumnsPercentage(this, 10, 25, 65);

                this.add(controlButtons, 0, 0);
                this.add(parameterNameTextField, 1, 0);
                this.add(parameterValueTextField, 2, 0);
            }

            public TextField getParameterNameTextField() {
                return parameterNameTextField;
            }

            public RichTextArea getParameterValueTextField() {
                return parameterValueTextField;
            }
        }
    }
}
