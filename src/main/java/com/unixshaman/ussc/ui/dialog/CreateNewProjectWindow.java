package com.unixshaman.ussc.ui.dialog;

import com.unixshaman.ussc.ui.mainmenu.MyFile;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.stage.DirectoryChooser;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.util.StringConverter;

import java.io.File;

import static com.unixshaman.ussc.ui.common.common.setColumnsPercentage;

public class CreateNewProjectWindow {

    private MyFile projectLocation = new MyFile(System.getProperty("user.home"));

    public MyFile display() {
        Stage window = new Stage();
        window.initModality(Modality.APPLICATION_MODAL);
        window.setTitle("Создать новый проект...");
        //window.setMinWidth(600);
        //window.setMinHeight(200);

        Label projectNameLabel = new Label("Название проекта: ");
        Label projectPathLabel = new Label("Расположение проекта: ");

        TextField projectNameTextField = new TextField();
        TextField projectPathTextField = new TextField();

        projectPathTextField.setText(projectLocation.getAbsolutePath());

        projectPathTextField.textProperty().bindBidirectional(projectNameTextField.textProperty(), new StringConverter<String>() {
            @Override
            public String toString(String object) {
                return (new File(projectLocation.getAbsolutePath(), object)).getAbsolutePath();
            }

            @Override
            public String fromString(String string) {
                return string.replace((new File(projectLocation.getAbsolutePath())).getAbsolutePath() + File.separator, "");
            }
        });


        Button selectProjectLocation = new Button("Выбрать расположение");
        selectProjectLocation.setOnAction(event -> {
            DirectoryChooser directoryChooser = new DirectoryChooser();
            File selectedDirectory = directoryChooser.showDialog(window);

            if(selectedDirectory != null) {
                projectLocation = new MyFile(selectedDirectory.getAbsolutePath());
                if (projectNameTextField.getText().equals(""))
                    projectPathTextField.setText(projectLocation.getAbsolutePath() + File.separator);
                else
                    projectPathTextField.setText((new File(projectLocation, projectNameTextField.getText()).getAbsolutePath()));
            }
        });

        Button createProject = new Button("Создать проект");
        createProject.setOnAction(event -> {
            projectLocation = new MyFile(projectPathTextField.getText() + File.separator);
            window.close();
        });

        Button cancel = new Button("Отмена");
        cancel.setOnAction(event -> {
            projectLocation = null;
            window.close();
        });

        HBox hb1 = new HBox(20, createProject, cancel);
        hb1.setAlignment(Pos.CENTER);

        GridPane gp1 = new GridPane();
        gp1.setPadding(new Insets(10, 10, 10,10 ));
        gp1.setHgap(8);
        gp1.setVgap(8);

        setColumnsPercentage(gp1, 25, 50, 25);

        gp1.add(projectNameLabel, 0, 0);
        gp1.add(projectNameTextField, 1, 0, 2, 1);
        gp1.add(projectPathLabel, 0, 1);
        gp1.add(projectPathTextField, 1, 1, 1,1);
        gp1.add(selectProjectLocation, 2, 1, 1, 1);

        VBox layout = new VBox(20);
        layout.getChildren().addAll(gp1, hb1);
        layout.setAlignment(Pos.CENTER);

        Scene scene = new Scene(layout, 600, 150);
        window.setScene(scene);
        window.showAndWait();

        return projectLocation;
    }

}
