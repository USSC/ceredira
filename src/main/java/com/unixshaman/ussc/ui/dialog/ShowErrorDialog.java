package com.unixshaman.ussc.ui.dialog;

import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.TextArea;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.stage.Modality;
import javafx.stage.Stage;

import java.io.PrintWriter;
import java.io.StringWriter;

public class ShowErrorDialog {

    public static void showErrorDialog(Throwable e) {
        StringWriter errorMsg = new StringWriter();
        e.printStackTrace(new PrintWriter(errorMsg));

        Stage dialog = new Stage();
        dialog.setTitle("В приложении произошла ошибка");
        dialog.initModality(Modality.APPLICATION_MODAL);

        TextArea errMsg = new TextArea();
        errMsg.setWrapText(true);
        ScrollPane sp = new ScrollPane(errMsg);
        sp.setFitToHeight(true);
        sp.setFitToWidth(true);
        BorderPane bp = new BorderPane();
        bp.setCenter(sp);

        Button closeButton = new Button("Закрыть");
        closeButton.setOnAction(event -> dialog.close());

        HBox hb = new HBox(20);
        hb.setAlignment(Pos.CENTER);
        hb.setPadding(new Insets(10, 10, 10, 10));
        hb.getChildren().addAll(closeButton);
        bp.setBottom(hb);

        errMsg.setText(errorMsg.toString());

        dialog.setScene(new Scene(bp, 600, 400));
        dialog.show();
    }
}
