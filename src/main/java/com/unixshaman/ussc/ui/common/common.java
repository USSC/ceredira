package com.unixshaman.ussc.ui.common;

import com.unixshaman.ussc.ApplicationContext;
import com.unixshaman.ussc.ui.editor.EnvironmentsEditor;
import com.unixshaman.ussc.ui.editor.OperationGroupsEditor;
import com.unixshaman.ussc.ui.editor.TestsEditor;
import com.unixshaman.ussc.ui.mainmenu.MyFile;
import com.unixshaman.ussc.ui.mainmenu.MyTreeCell;
import javafx.collections.ObservableList;
import javafx.scene.Node;
import javafx.scene.control.TitledPane;
import javafx.scene.control.TreeItem;
import javafx.scene.control.TreeView;
import javafx.scene.layout.ColumnConstraints;
import javafx.scene.layout.GridPane;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

/**
 * Библиотека частоиспользуемых функций
 */
public class common {

    public static void findFiles(TreeView<MyFile> filesTree, MyFile dir, TreeItem<MyFile> parent) {
        TreeItem<MyFile> root = new TreeItem<>(dir);

        root.setExpanded(true);
        //try {
        MyFile[] files = dir.listFiles();
        for (MyFile file : files) {
            if (file.isDirectory()) {
                findFiles(filesTree, file, root);
            } else {
                root.getChildren().add(new TreeItem<>(file));
            }
        }
        if(parent == null){
            root.setValue(dir);
            filesTree.setRoot(root);
        } else {
            parent.getChildren().add(root);
        }
        //} catch (IOException e) {
        //    e.printStackTrace();
        //}
    }

    public static List<MyFile> getFilesInFolder(MyFile folder, boolean recursive) {

        List<MyFile> resultList = new ArrayList<>();

        // get all the files from a directory
        MyFile[] fList = folder.listFiles();
        resultList.addAll(Arrays.asList(fList));

        if (recursive) {
            for (MyFile file : fList) {
                //if (file.isFile()) {
                //    System.out.println(file.getAbsolutePath());
                //} else
                if (file.isDirectory()) {
                    resultList.addAll(getFilesInFolder(file, true));
                }
            }
        }

        return resultList;
    }

    public static TreeView<MyFile> initProjectExplorer(MyFile projectPathFile) {
        TreeItem<MyFile> root = new TreeItem<>(projectPathFile);
        root.setExpanded(true);
        TreeView<MyFile> tree = new TreeView(root);

        tree.setCellFactory(arg0 -> new MyTreeCell());

        // TODO: Сделать открытие файлов преокта по двойному клику
        //tree.setOnMouseClicked(event -> System.out.println("Click"));

        common.findFiles(tree, projectPathFile,null);
        return tree;
    }

    public static void refreshProjectTree() {
        ApplicationContext.getInstance().getApplication().getMainLeftPane().getProjectExplorerTab().setContent(initProjectExplorer(new MyFile(ApplicationContext.getInstance().getProjectPath().toString())));
    }

    public static void createNewOperationGroup() {
        ApplicationContext.getInstance().getApplication().getMainTabsList().getTabs().add(new OperationGroupsEditor(ApplicationContext.getInstance().getApplication()));
    }

    public static void openOperationsGroup(String pathToGroup) {
        ApplicationContext.getInstance().getApplication().getMainTabsList().getTabs().add(new OperationGroupsEditor(ApplicationContext.getInstance().getApplication(), pathToGroup));
    }

    public static void createNewEnvironment() {
        ApplicationContext.getInstance().getApplication().getMainTabsList().getTabs().add(new EnvironmentsEditor(ApplicationContext.getInstance().getApplication()));
    }

    public static void openEnvironment(String pathToEnvironment) {
        ApplicationContext.getInstance().getApplication().getMainTabsList().getTabs().add(new EnvironmentsEditor(ApplicationContext.getInstance().getApplication(), pathToEnvironment));
    }

    public static void createNewTest() {
        ApplicationContext.getInstance().getApplication().getMainTabsList().getTabs().add(new TestsEditor().crateNewTestsEditor());
    }

    public static void openTest(String pathToTest) {
        ApplicationContext.getInstance().getApplication().getMainTabsList().getTabs().add(new TestsEditor().openTestsEditor(pathToTest));
    }

    public static <T extends Node> void moveObjectUp(ObservableList<T> collection, T currentObj) {
        int index = collection.indexOf(currentObj);
        if (index != 0) {
            swap(collection, index, index - 1);
        }
    }

    public static <T extends Node>  void moveObjectDown(ObservableList<T> collection, T currentObj) {
        int index = collection.indexOf(currentObj);
        if (index != collection.size() - 1) {
            swap(collection, index, index + 1);
        }
    }

    public static <T> void swap(ObservableList<T> list, int i, int j) {
        List<T> newContent = new ArrayList<T>(list);
        Collections.swap(newContent, i, j);
        list.setAll(newContent);
    }

    public static <T extends Node> void insertBefore(ObservableList<T> collection, T currObj, T newObj) {
        if (collection.size() == 0) {
            collection.add(newObj);
        } else {
            int index = collection.indexOf(currObj);
            if (index == 0) {
                collection.add(0, newObj);
            } else {
                collection.add(index, newObj);
            }
        }
    }

    public static <T extends Node> void insertAfter(ObservableList<T> collection, T currObj, T newObj) {
        if (collection.size() == 0) {
            collection.add(newObj);
        } else {
            int index = collection.indexOf(currObj);
            if (collection.size() - 1 == index) {
                collection.add(newObj);
            } else {
                collection.add(index + 1, newObj);
            }
        }
    }

    public static <T1 extends Node, T2 extends TitledPane> void setExpandedForAll(ObservableList<T1> collection, Boolean value) {
        for (Node oper : collection) {
            ((T2)oper).setExpanded(value);
        }
    }

    public static void setColumnsPercentage(GridPane gp, int... percents) {
        for (int i = 0; i < percents.length; ++i) {
            ColumnConstraints column = new ColumnConstraints();
            column.setPercentWidth(percents[i]);
            gp.getColumnConstraints().add(column);
        }
    }

}
