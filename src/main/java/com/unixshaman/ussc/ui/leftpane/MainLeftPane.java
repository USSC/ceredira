package com.unixshaman.ussc.ui.leftpane;

import com.unixshaman.ussc.Ceredira;
import javafx.geometry.Point3D;
import javafx.geometry.Side;
import javafx.scene.control.Label;
import javafx.scene.control.Tab;
import javafx.scene.control.TabPane;

public class MainLeftPane extends TabPane {

    private Tab projectExplorerTab;

    private Ceredira application;

    public MainLeftPane(Ceredira application) {
        this.application = application;
        this.setId("leftPaneTabs");
        this.setSide(Side.LEFT);
        this.prefWidthProperty().setValue(200);
        this.setMaxWidth(400);
        this.setMinWidth(29);
        this.getTabs().addAll(projectExplorerTab());
    }

    private Tab projectExplorerTab() {
        projectExplorerTab = new Tab();
        projectExplorerTab.setId("projectExplorerTab");

        Label lb1 = new Label("Проект");
        lb1.setRotationAxis(new Point3D(0, 0, 0));
        lb1.setRotate(0);

        lb1.onMouseClickedProperty().set(e -> {
            if (application.getMainHorisontalSplitPane().getDividers().get(0).getPosition() > 0.035) {
                application.getMainHorisontalSplitPane().getDividers().get(0).setPosition(0.0);
            }
            else {
                application.getMainHorisontalSplitPane().getDividers().get(0).setPosition(0.25);
            }
        });

        projectExplorerTab.setGraphic(lb1);

        return projectExplorerTab;
    }

    public Tab getProjectExplorerTab() {
        return projectExplorerTab;
    }
}
