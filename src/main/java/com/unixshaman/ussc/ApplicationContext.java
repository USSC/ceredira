package com.unixshaman.ussc;

import java.nio.file.Path;
import java.nio.file.Paths;

public class ApplicationContext {

    private static volatile ApplicationContext instance;

    private ApplicationContext() {}

    public static ApplicationContext getInstance() {
        ApplicationContext localInstance = instance;
        if (localInstance == null) {
            synchronized (ApplicationContext.class) {
                localInstance = instance;
                if (localInstance == null) {
                    instance = localInstance = new ApplicationContext();
                }
            }
        }
        return localInstance;
    }



    private Ceredira application;
    private Path projectPath;
    private Path environmentsPath;
    private Path testsPath;
    private Path operationsPath;
    private boolean projectOpened = false;

    public Ceredira getApplication() {
        return application;
    }

    public void setApplication(Ceredira application) {
        this.application = application;
    }

    public Path getProjectPath() {
        return projectPath;
    }

    public void setProjectPath(Path projectPath) {
        if (projectPath != null) {
            this.projectPath = projectPath;
            this.environmentsPath = Paths.get(this.projectPath.toString(), "Environments");
            this.testsPath = Paths.get(this.projectPath.toString(), "Tests");
            this.operationsPath = Paths.get(this.projectPath.toString(), "Operations");
            projectOpened = true;
        } else {
            this.projectPath = null;
            this.environmentsPath = null;
            this.testsPath = null;
            this.operationsPath = null;
            projectOpened = false;
        }
    }

    public boolean isProjectOpened() {
        return projectOpened;
    }

    public void setProjectOpened(boolean projectOpened) {
        this.projectOpened = projectOpened;
    }

    public Path getEnvironmentsPath() {
        return environmentsPath;
    }

    public Path getTestsPath() {
        return testsPath;
    }

    public Path getOperationsPath() {
        return operationsPath;
    }
}
