package com.unixshaman.ussc.operations;

import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.XmlValue;

/**
 * Created by gdjamalov on 17.07.2017.
 */
@XmlAccessorType()
@XmlType(name = "parameter", propOrder = {"name", "title", "description", "direction", "defaultValue"})
public class Parameter {

    private String name;
    private String title;
    private String description;
    private Direction direction;
    private String defaultValue;

    public String getName() {
        return name;
    }

    @XmlAttribute
    public void setName(String name) {
        this.name = name;
    }

    public String getTitle() {
        return title;
    }

    @XmlAttribute
    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    @XmlAttribute
    public void setDescription(String description) {
        this.description = description;
    }

    public Direction getDirection() {
        return direction;
    }

    @XmlAttribute
    public void setDirection(Direction direction) {
        this.direction = direction;
    }

    public String getdefaultValue() {
        return defaultValue;
    }

    @XmlValue
    public void setdefaultValue(String value) {
        this.defaultValue = value;
    }

    public Parameter() {

    }

    public Parameter(String name, String title, String description, Direction direction, String defaultValue) {
        this.name = name;
        this.title = title;
        this.description = description;
        this.direction = direction;
        this.defaultValue = defaultValue;
    }
}
