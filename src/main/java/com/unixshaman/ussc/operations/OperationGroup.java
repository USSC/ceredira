package com.unixshaman.ussc.operations;

import com.unixshaman.ussc.ui.dialog.ShowErrorDialog;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import javax.xml.bind.annotation.*;
import java.io.File;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by gdjamalov on 18.07.2017.
 */
@XmlAccessorType()
@XmlRootElement(name = "group")
@XmlType(name = "group", propOrder = {"name", "title", "description", "fileName", "operations"})
public class OperationGroup {

    private String name;
    private String title;
    private String description;
    private String fileName;
    private List<Operation> operations = new ArrayList<>();

    public String getName() {
        return name;
    }

    @XmlAttribute
    public void setName(String name) {
        this.name = name;
    }

    public String getTitle() {
        return title;
    }

    @XmlAttribute
    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    @XmlAttribute
    public void setDescription(String description) {
        this.description = description;
    }

    public String getFileName() {
        return fileName;
    }

    @XmlAttribute
    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public List<Operation> getOperations() {
        return operations;
    }

    @XmlElement(name="operation")
    public void setOperations(List<Operation> operations) {
        this.operations = operations;
    }

    public void addOperations(List<Operation> operations) {
        this.operations.addAll(operations);
    }

    public OperationGroup() {
    }

    public OperationGroup(String fileName) {
        this.fileName = fileName;

        try {

            File file = new File(fileName);
            JAXBContext jaxbContext = JAXBContext.newInstance(OperationGroup.class);

            Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();
            OperationGroup operationGroup = (OperationGroup) jaxbUnmarshaller.unmarshal(file);

            this.name = operationGroup.name;
            this.title = operationGroup.title;
            this.description = operationGroup.description;
            this.fileName = operationGroup.fileName;
            this.operations = operationGroup.operations;

        } catch (JAXBException e) {
            ShowErrorDialog.showErrorDialog(e);
        }
    }

    public void saveGroup() {
        saveGroup(this.fileName);
    }

    public void saveGroup(String fileName) {
        try {
            File file = new File(fileName);
            JAXBContext jaxbContext = JAXBContext.newInstance(OperationGroup.class);
            Marshaller jaxbMarshaller = jaxbContext.createMarshaller();

            jaxbMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);

            jaxbMarshaller.marshal(this, file);
        } catch (JAXBException e) {
            ShowErrorDialog.showErrorDialog(e);
        }
    }
}
