package com.unixshaman.ussc.operations;

/**
 * Created by gdjamalov on 17.07.2017.
 */
public enum Direction {
    IN, OUT, CONST
}
