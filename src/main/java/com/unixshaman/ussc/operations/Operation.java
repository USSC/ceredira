package com.unixshaman.ussc.operations;

import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by gdjamalov on 17.07.2017.
 */
@XmlAccessorType()
@XmlType(name = "operation", propOrder = {"name", "title", "shortDescription", "detailedDescription", "exampleParameters", "parameters", "code"})
public class Operation {

    private String name;
    private String title;
    private String shortDescription;
    private String detailedDescription;
    private String exampleParameters;
    private List<Parameter> parameters = new ArrayList<>();
    private String code;

    public String getName() {
        return name;
    }

    @XmlAttribute
    public void setName(String name) {
        this.name = name;
    }

    public String getTitle() {
        return title;
    }

    @XmlAttribute
    public void setTitle(String title) {
        this.title = title;
    }

    public String getShortDescription() {
        return shortDescription;
    }

    @XmlAttribute
    public void setShortDescription(String shortDescription) {
        this.shortDescription = shortDescription;
    }

    public String getDetailedDescription() {
        return detailedDescription;
    }

    @XmlAttribute
    public void setDetailedDescription(String detailedDescription) {
        this.detailedDescription = detailedDescription;
    }

    public String getExampleParameters() {
        return exampleParameters;
    }

    @XmlAttribute
    public void setExampleParameters(String exampleParameters) {
        this.exampleParameters = exampleParameters;
    }

    public List<Parameter> getParameters() {
        return parameters;
    }

    @XmlElement(name="parameter")
    public void setParameters(List<Parameter> parameters) {
        this.parameters = parameters;
    }

    public void addParameters(List<Parameter> parameters) {
        this.parameters.addAll(parameters);
    }

    public String getCode() {
        return code;
    }

    @XmlElement
    public void setCode(String code) {
        this.code = code;
    }
}
