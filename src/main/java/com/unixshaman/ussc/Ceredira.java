package com.unixshaman.ussc;

import com.unixshaman.ussc.ui.dialog.ShowErrorDialog;
import com.unixshaman.ussc.ui.leftpane.MainLeftPane;
import com.unixshaman.ussc.ui.mainmenu.MainMenuBar;
import javafx.application.Application;
import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Orientation;
import javafx.scene.Scene;
import javafx.scene.control.SplitPane;
import javafx.scene.control.TabPane;
import javafx.scene.layout.BorderPane;
import javafx.stage.Stage;

/**
 * Created by gdjamalov on 17.07.2017.
 */

/**
 * Основной класс данного приложения, наследник класса Application, для
 * возможности отрисовки графического интерфейса пользователя
 */
public class Ceredira extends Application implements EventHandler<ActionEvent> {

    /**
     * Корневой элемент приложения, находящийся в окне
     */
    private BorderPane mainBorderPane;

    /**
     * Разделим окно по вертикали на 2 части, чтобы
     * можно было в центре разместить левые панели, основное пространство приложения
     * и правые панели, а в нижней части были панели типа: события, логирование...
     */
    private SplitPane mainVerticalSplitPane;

    /**
     * Это разделитель основной части окна, на 3 части,
     * под левые панели, основную часть и правые панели
     */
    private SplitPane mainHorisontalSplitPane;

    /**
     * Основная часть приложения - пространство под табы
     */
    private TabPane mainTabsList;

    /**
     * Панель с вкладка слева в главном окне
     */
    private MainLeftPane mainLeftPane;

    /**
     * Главная сцена приложения
     */
    private Scene mainScene;

    /**
     * Главное окно приложения
     */
    private Stage mainStage;

    /**
     * Контекст всего приложения
     */
    private ApplicationContext applicationContext;

    /**
     * Основная точка входа в приложение
     * @param args Аргументы командной строки
     */
    public static void main(String[] args) {
        launch(args);
    }


    /**
     * Оснавная функция запуска оконного приложения
     * @param primaryStage Основное окно, для этого приложения
     * @throws Exception
     */
    @Override
    public void start(Stage primaryStage) throws Exception {

        Thread.setDefaultUncaughtExceptionHandler(Ceredira::showError);

        applicationContext = ApplicationContext.getInstance();
        applicationContext.setApplication(this);

        primaryStage.setTitle("Ceredira - testing automation tool");

        mainVerticalSplitPane = new SplitPane();
        mainVerticalSplitPane.setId("mainVerticalSplitPane");
        mainVerticalSplitPane.setOrientation(Orientation.VERTICAL); // Указание вертикальной ориентации

        mainHorisontalSplitPane = new SplitPane();
        mainHorisontalSplitPane.setId("mainHorisontalSplitPane");
        mainHorisontalSplitPane.setOrientation(Orientation.HORIZONTAL); // Указание горизонтальной ориентации

        mainVerticalSplitPane.getItems().add(mainHorisontalSplitPane); // Пока добавили в вертикальный разделитель только основное окно
        // TODO: Добавить нижние панели

        mainTabsList = new TabPane();
        mainTabsList.setId("mainTabsList");

        mainLeftPane = new MainLeftPane(this);

        mainHorisontalSplitPane.getItems().addAll(mainLeftPane, mainTabsList); // Добавили левые панели и основную часть
        // TODO: Добавить правые панели

        mainHorisontalSplitPane.setDividerPositions(0.25); // 25% от общего пространства будут занимать правые панели

        mainBorderPane = new BorderPane();
        mainBorderPane.setId("mainBorderPane");
        mainBorderPane.setTop(new MainMenuBar(this)); // Добавили основное меню вверх окна
        mainBorderPane.setCenter(mainVerticalSplitPane); // Добавили вертикальный разделитель в центр окна

        mainScene = new Scene(mainBorderPane, 1000, 800); // Размер приложения по умолчанию при старте
        mainStage = primaryStage;
        primaryStage.setScene(mainScene);
        primaryStage.show();
    }

    /**
     * Функция перехвата действий пользователя, не удалять!!!
     * @param event Пользовательское событие
     */
    @Override
    public void handle(ActionEvent event) {

    }

    public static void showError(Thread t, Throwable e) {
        if (Platform.isFxApplicationThread()) {
            ShowErrorDialog.showErrorDialog(e);
        } else {
            System.out.println("An unexpected error occurred in " + t);
            System.out.println(e.getStackTrace());
        }
    }

    public BorderPane getMainBorderPane() {
        return mainBorderPane;
    }

    public SplitPane getMainVerticalSplitPane() {
        return mainVerticalSplitPane;
    }

    public SplitPane getMainHorisontalSplitPane() {
        return mainHorisontalSplitPane;
    }

    public TabPane getMainTabsList() {
        return mainTabsList;
    }

    public Scene getMainScene() {
        return mainScene;
    }

    public MainLeftPane getMainLeftPane() {
        return mainLeftPane;
    }

    public Stage getMainStage() {
        return mainStage;
    }

    public ApplicationContext getApplicationContext() {
        return applicationContext;
    }
}